FROM centos:7
ENV PATH=/opt/rh/rh-python38/root/usr/local/bin:/opt/rh/rh-python38/root/usr/bin${PATH:+:${PATH}}
ENV LD_LIBRARY_PATH=/opt/rh/rh-python38/root/usr/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
RUN \
	yum install -y centos-release-scl && \
	yum install -y rh-python38-python rh-python38-python-pip && \
	pip install flask flask_restful flask_jsonpify && \
	mkdir /python_api
COPY python-api.py /python_api/python-api.py
EXPOSE 5290
ENTRYPOINT ["python3", "/python_api/python-api.py"]
